package wk1;

//<editor-fold desc="Outcomes: Interfaces">
/*
  + Use the Collection<E> and List<E> interfaces defined in the Java Collection Framework
  + Explain when to use Collection<E> instead of List<E> and vice versa
  + Demonstrate correct use of generics when declaring Collection<E> and List<E> interfaces
  + Describe the implications of an interface extending another interface
  + List two classes that implement the Collection<E> interface
  + List two classes that implement the List<E> interface
 */
//</editor-fold>

//<editor-fold desc="Outcomes: Array Based Lists">
/*
  + Describe key differences between an array and an ArrayList<E> object
  + Implement classes and methods that make use of generics
  + Write an array-based implementation of the List<E> interface, including the following methods:
    - add(E)
    - add(int, E)
    - clear()
    - contains(Object)
    - get(int)
    - indexOf(Object)
    - isEmpty()
    - remove(int)
    - remove(Object)
    - set(int, E)
    - size()
    - toArray()
  + Implement small software systems that use one or more ArrayList<E> objects
  + Describe key differences between the in class implementation and the java.util.ArrayList<E> implementation
  + Describe differences in the java.util.ArrayList implementation compared to the one created in lecture that affect the asymptotic time complexity of any of the methods
 */
//</editor-fold>

//<editor-fold desc="Outcomes: Big-O Notation and Algorithm Efficiency">
/*
   - Explain the purpose of Big-O notation
   - Describe the limitations of Big-O notation
   - Be familiar with the formal definition of Big-O
   - Using Big-O notation, determine the asymptotic time complexity of an algorithm with a conditional
   - Using Big-O notation, determine the asymptotic time complexity of an algorithm with a loop
   - Determine the asymptotic time complexity of an algorithm with a nested loop
   - Using Big-O notation, determine the asymptotic time complexity of an algorithm that calls other methods with known asymptotic time complexity
   - Use time complexity analysis to choose between two competing algorithms
   - Describe the meaning of the following symbols: T(n), f(n), and O(f(n))
   - Given T(n) expressed as a polynomial, determine the Big-O notation
   - Determine the asymptotic time complexity of the following methods from the ArrayList<E> class:
     - add(E)
     - add(int, E)
     - clear()
     - contains(Object)
     - get(int)
     - indexOf(Object)
     - isEmpty()
     - remove(int)
     - remove(Object)
     - set(int, E)
     - size()
 */
//</editor-fold>

//<editor-fold desc="Outcomes: Linked Lists">
/*
  - Describe key differences between an array based list and a linked list
  - Describe advantages and disadvantages of a singly linked list verses a doubly linked list
  - Write an singly linked list implementation of the List<E> interface, including the following methods:
    - add(E)
    - add(int, E)
    - clear()
    - contains(Object)
    - get(int)
    - indexOf(Object)
    - isEmpty()
    - remove(int)
    - remove(Object)
    - set(int, E)
    - size()
    - toArray()
  - Describe key differences between a singly linked list and the LinkedList<E> class
  - Determine the asymptotic time complexity of the following methods from a singly linked list class developed in lecture: add(E), add(int, E), clear(), contains(Object), get(int), indexOf(Object), isEmpty(), remove(int), remove(Object), set(int, E), and size()
  - Describe differences in the JCF LinkedList implementation compared to the one created in lecture that affect the asymptotic time complexity of any of the methods
  - Implement small software systems that use one or more LinkedList<E> objects
 */
//</editor-fold>

import y17q3.cs2852.msoe.edu.ArrayList;

//import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Driver {
    public static void main(String[] args) {
        Collection<String> words = new java.util.ArrayList<>();
        words.add("egg");
        words.add("pig");
        words.add("half");
        words.add("animals");
        int sum = 0;
        for(String word : words) {
            if(word instanceof String) {
                sum += word.length();
            }
        }
        System.out.println("The total number of characters in words is:" + sum);

    }
}
