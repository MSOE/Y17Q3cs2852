package wk6;

import y17q3.cs2852.msoe.edu.BinarySearchTree;

public class Driver {
    public static void main(String[] args) {
        BinarySearchTree<String> tree = new BinarySearchTree<>();
        tree.add("E");
        tree.add("B");
        tree.add("A");
        tree.add("G");
        System.out.println(tree.max());
    }
}
