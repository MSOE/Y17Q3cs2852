package wk8;

import y17q3.cs2852.msoe.edu.HashTable;

public class Driver2 {
    public static void main(String[] args) {
        HashTable<String> table = new HashTable<>();
        System.out.println(table.size());
        System.out.println(table.contains(""));
        table.add("egg");
        System.out.println(table.size());
        table.add("pig");
        System.out.println(table.size());
        System.out.println(table.contains("pig"));
        table.add("half");
        System.out.println(table.size());
        table.add("animals");
        System.out.println(table.size());
        for(String word : table) {
            System.out.println(word);
        }
    }
}
