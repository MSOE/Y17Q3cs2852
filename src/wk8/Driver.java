package wk8;

import y17q3.cs2852.msoe.edu.PureStack;
import y17q3.cs2852.msoe.edu.Stack;

import java.util.EmptyStackException;

public class Driver {
    public static void main(String[] args) {
        test("((a + b) + c)", true);
        test("([a + b] + c)", true);
        test("(a + {b + c})", true);
        test("((a + b) + c", false);
        test("([a + b] + c]", false);
        test("(a + {b + c]}", false);
        test("(a + b) + c]}", false);
    }

    private static void test(String expression, boolean expected) {
        if(parenChecker(expression)!=expected) {
            System.out.println(expression + " should return "
                    + expected);
        }
    }

    public static boolean parenChecker(String expression) {
        PureStack<Character> stack = new Stack<>();
        boolean good = true;
        for(int i=0; good && i<expression.length(); ++i) {
            try {
                char character = expression.charAt(i);
                switch (character) {
                    case '(':
                    case '[':
                    case '{':
                        stack.push(character);
                        break;
                    case ')':
                        good = '(' == stack.pop();
                        break;
                    case ']':
                        good = '[' == stack.pop();
                        break;
                    case '}':
                        good = '{' == stack.pop();
                        break;
                }
            } catch(EmptyStackException e) {
                good = false;
            }
        }
        if(good) {
            good = stack.empty();
        }
        return good;
    }

}
