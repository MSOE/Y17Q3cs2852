package wk5;

//<editor-fold desc="Outcomes: Recursion">
/*
  * For a given input, determine how many times a recursive method will call itself
  * Explain the role of the base case and recursive step in recursive algorithms
  * Use recursion to traverse a list
  * Use recursion to search a sorted array
  * Understand and apply recursion in algorithm development
 */
//</editor-fold>
//<editor-fold desc="Examples">
/* Examples:
   factorial(int n) -> 0, 1, 2, 6
   fibonacci(int n) -> 0, 1, 1, 2
   countX(String str) | "xxhixx" -> 4
   powerN(double base, int n)
   LinkedList.size()
   binarySearch(int[] data, int target)
 */
//</editor-fold>

import java.util.ArrayList;
import java.util.List;

public class Driver {

    public static void main3(String[] args) {
        System.out.println(facRec(1));
        System.out.println(facRec(4));
    }

    public static long facRec(int n) {
        if(n<=1) {
            return 1;
        }
        return n * facRec(n-1);
    }

    public static long factorial(int n) {
        long answer = 1;
        for(int i=n; i>1; --i) {
            answer *= i;
        }
        return answer;
    }

    public static void main2(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(4);
        list.add(6);
        list.add(8);
        list.add(10);
        list.add(12);
        list.add(14);
        System.out.println(binarySearchRec(list, 3));
        System.out.println(binarySearchRec(list, 8));
        System.out.println(binarySearchRec(list, 4));
        System.out.println(binarySearchRec(list, 12));
        System.out.println(binarySearchRec(list, 14));

    }

    /*
    Now passes tests
     */
    public static <E extends Comparable<E>> boolean binarySearch(List<E> list, E target) {
        boolean found = false;
        int start = 0;
        int end = list.size();
        while(!found && start!=end) {
            int index = (start+end)/2;
            int comparison = target.compareTo(list.get(index));
            if(comparison<0) {
                end = index;
            } else if(comparison>0) {
                start = index+1;
            } else {
                found = true;
            }
        }
        return found;
    }

    public static <E extends Comparable<E>> boolean binarySearchR(List<E> list, E target) {
        return binarySearchR(list, target, 0, list.size());
    }

    private static <E extends Comparable<E>> boolean binarySearchR(List<E> list, E target, int start, int end) {
        if(start==end) {
            return false;
        }

        boolean found = true;
        int index = (start+end)/2;
        int comparison = target.compareTo(list.get(index));
        if(comparison<0) {
            found = binarySearchR(list, target, start, index);
        } else if(comparison>0) {
            found = binarySearchR(list, target, index+1, end);
        }
        return found;
    }

    public static <E extends Comparable<E>> boolean binarySearchRec(List<E> list, E target) {
        if(list.size()==0) {
            return false;
        }

        int comparison = target.compareTo(list.get(list.size()/2));
        if(comparison<0) {
            return binarySearchRec(list.subList(0, list.size()/2), target);
        }
        if(comparison>0) {
            return binarySearchRec(list.subList(list.size()/2 + 1, list.size()), target);
        }
        return true;
    }


    public static void main22(String[] args) {
        System.out.println(count7(717));
        System.out.println(count7(7));
        System.out.println(count7(123));
        System.out.println(count7(7777777));
        System.out.println(count7(70701277));
    }

    /**
     * To be used for a debugging exercise...
     * Given a non-negative int n, return the count of the occurrences of 7 as a digit,
     * Examples:
     *  *      717 yields 2
     *  *        7 yields 1
     *  *      123 yields 0
     *  *  7777777 yields 7
     *  * 70701277 yields 4
     *
     * @param n Non-negative integer
     * @return the number of occurrences of 7 as a digit in n
     */
    public static int count7(int n) {
        int count = 0;
        if(n>0) {
            int digit = n%10;
            boolean isDivisible = digit==7;
            count = (isDivisible ? 1 : 0) + count7(n/10);
        }
        return count;
    }
}
