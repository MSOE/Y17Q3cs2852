package y17q3.cs2852.msoe.edu;

import java.util.List;
import java.util.ListIterator;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

//<editor-fold desc="Outcomes: Testing">
/*
  - Describe the following levels of testing: unit, integration, system, and acceptance
  - Describe the differences between black-box testing and white-box testing
  - List advantages and disadvantages of black-box testing verses white-box testing
  - Develop tests that test boundary conditions
*/
//</editor-fold>

public class LinkedList<E> implements List<E> {

    private Node head;

    private class Node {
        E value;
        Node next;

        public Node(E value, Node next) {
            this.value = value;
            this.next = next;
        }

        public Node(E value) {
            this(value, null);
        }
    }

    public LinkedList() {
        head = null;
    }

    @Override
    public boolean isEmpty() {
        return head==null;
    }

    @Override
    public int size() {
        int size = 0;
        Node walker = head;
        while(walker!=null) {
            ++size;
            walker = walker.next;
        }
        return size;
    }

    @Override
    public boolean contains(Object target) {
        return indexOf(target)>=0;
    }

    @Override
    public int indexOf(Object target) {
        int index = -1;
        for(int i=0; index==-1 && i<size(); ++i) {
            if((target==null ? get(i)==null : target.equals(get(i)))) {
                index = i;
            }
        }
        return index;
    }

    @Override
    public void clear() {
        clear1();
    }

    @Override
    public E get(int index) {
        return get1(index);
    }

    @Override
    public E set(int index, E element) {
        return set1(index, element);
    }

    @Override
    public boolean add(E element) {
        return add2(element);
    }

    @Override
    public boolean remove(Object target) {
        return remove1(target);
    }

    @Override
    public void add(int index, E element) {
        add1(index, element);
    }

    @Override
    public E remove(int index) {
        return remove1(index);
    }

    @Override
    public Object[] toArray() {
        return toArray1();
    }

    //<editor-fold desc="clear()">
    public void clear1() {
        head = null;
    }

    public void clear2() {
        head.next = null;
    }

    public void clear3() {
        for (int i = 0; i < size(); i++) {
            remove(i);
        }
    }
    //</editor-fold>

    //<editor-fold desc="get(int)">
    public E get1(int index) {
        return getNode(index).value;
    }

    private Node getNode(int index) {
        if(index<0) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size());
        }
        try {
            Node walker = head;
            while(index>0) {
                walker = walker.next;
                --index;
            }
            if(walker==null) {
                throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + index);
            }
            return walker;
        } catch(NullPointerException e) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size());
        }
    }

    public E get2(int index) {
        Node walker = head;
        for (int i = 0; i < index; ++i){
            walker = walker.next;
        }
        return (E)walker;
    }

    public E get3(int index) {
        Node walker = head;
        int walking = 0;
        while(walking<index){
            walker = walker.next;
        }
        return walker.value;
    }

    public E get4(int index) {
        Iterator<E> itr = this.iterator();
        E output = null;
        for(int i=0; i<=index; i++) {
            output = itr.next();
        }
        return output;
    }

    public E get5(int index) {
        Node walker = head;
        for(int i = 0; i<size(); ++i) {
            if (i == index) {
                return walker.value;
            }
            walker = walker.next;
        }
        return null;
    }
    //</editor-fold>

    //<editor-fold desc="set(int, E)">
    public E set1(int index, E element) {
        Node node = getNode(index);
        E oldValue = node.value;
        node.value = element;
        return oldValue;
    }

    public E set2(int index, E element) {
        Node walker = head;
        for (int i = 0; i < index; ++i){
            walker = walker.next;
        }
        E oldValue = (E)walker;
        walker.value = element;
        return oldValue;
    }

    public E set3(int index, E element) {
        Node walker = head;
        for(int i = 0; i<index; ++i){
            walker = walker.next;
            if(indexOf(walker) == index){
                walker.value = element;
            }
        }
        return walker.value;
    }
    //</editor-fold>

    //<editor-fold desc="add(E)">
    public boolean add1(E element) {
        if(isEmpty()) {
            head = new Node(element);
        } else {
            Node nodeBefore = getNode(size()-1);
            nodeBefore.next = new Node(element);
        }
        return true;
    }

    public boolean add2(E e) {
        Node walker = head;
        while (walker.next != null){
            walker = walker.next;
        }
        walker.next = new Node(e, null);
        return true;
    }

    public boolean add3(E e) {
        Node walker = head;
        int size = size();

        for (int i = 0; i < size; ++i) {
            walker = walker.next;
        }

        walker.next = new Node(e);

        return true;
    }

    public boolean add4(E e) {
        Node walker = head;
        while(walker!=null) {
            walker = walker.next;
        }
        walker.value = e;
        return true;
    }
    //</editor-fold>

    //<editor-fold desc="remove(Object)">
    public boolean remove1(Object target) {
        int targetIndex = indexOf(target);
        if(targetIndex>=0) {
            remove(targetIndex);
        }
        return targetIndex>=0;
    }

    public boolean remove2(Object target) {
        remove(indexOf(target));
        return true;
    }

    public boolean remove3(Object o) {
        boolean successful = false;
        int index = indexOf(o);

        if (index >= 0) {
            remove(index);
            successful = true;
        }

        return successful;
    }

    public boolean remove4(Object o) {
        boolean changed = false;
        Node walker;
        Node temp;
        walker = head;
        if (head.equals(o) && head != null) {
            if(head.next == null) {
                head = null;
            } else {
                head = head.next;
            }
            changed = true;
        }
        while(walker.next != null) {
            temp = walker;
            walker = walker.next;
            if(walker.equals(o)) {
                temp.next = walker.next;
                changed = true;
            }
        }
        return changed;
    }

    public boolean remove5(Object o) {
        int position = indexOf(o);
        boolean removed = false;
        if(position != -1){
            Node nodeBefore = head;
            Node nodeAfter = head;
            for(int i = 0; i < position + 1; i++){
                if(i == position - 1){
                    nodeBefore = nodeAfter;
                }
                nodeAfter = nodeAfter.next;
            }
            nodeBefore.next = nodeAfter;
            removed = true;
        }
        return removed;
    }

    public boolean remove6(Object o) {
        Node walker = head;
        Node walkerPrev = new Node(null,head);
        while ( walker.next != null){
            if(walker.equals(o) ){
                walkerPrev.next = walker.next;
            }
            walkerPrev = walker;
            walker = walker.next;
        }

        return true;
    }
    //</editor-fold>

    //<editor-fold desc="add(int, E)">
    public void add1(int index, E element) {
        if(index==0) {
            head = new Node(element, head);
        } else {
            Node nodeBefore = getNode(index-1);
            nodeBefore.next = new Node(element, nodeBefore.next);
        }
    }

    public void add2(int index, E element) {
        Node walker = head;
        for (int i = 0; i < index-1; ++i){
            walker = walker.next;
        }
        walker.next = new Node(element, walker.next);
    }

    public void add3(int index, E element) {
        Node addedNode = new Node(element, null);
        Node previous = head;
        Node next = head;
        for(int i = 0; i < index + 1; i++){
            next = next.next;
            if(i == index - 1){
                previous = next;
            }
        }
        previous.next = addedNode;
        addedNode.next = next;
    }

    public void add4(int index, E element) {
        Node walker = head;
        for(int i=0; i<index-2; i++) {
            walker = walker.next;
        }

        Node previous = walker.next;
        Node after = walker.next;
        previous.next = new Node(element, after);
    }

    public void add5(int index, E element) {
        if (index == 0)
            head = new Node(element, null);
        else {
            Node walker = head;
            if (index <= size()) {
                for (int i = 0; i < index - 1; ++i) {
                    walker = walker.next;
                }
                Node newNode = new Node(element, walker.next);
                walker.next = newNode;
            }
        }
    }
    //</editor-fold>

    //<editor-fold desc="remove(int)">
    public E remove1(int index) {
        if(isEmpty()) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size());
        }
        E oldValue;
        if(index==0) {
            oldValue = head.value;
            head = head.next;
        } else {
            Node nodeBefore = getNode(index-1);
            if(nodeBefore.next==null) {
                throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size());
            }
            oldValue = nodeBefore.next.value;
            nodeBefore.next = nodeBefore.next.next;
        }
        return oldValue;
    }

    public E remove2(int index) {
        Node walker = head;
        for (int i = 0; i < index-1; ++i){
            walker = walker.next;
        }
        E oldValue = walker.next.value;
        walker.next = walker.next.next;
        return oldValue;
    }

    public E remove3(int index) {
        if(index > size() - 1 || index < 0){
            throw new IndexOutOfBoundsException();
        }
        E value = null;
        if(index != -1){
            Node nodeBefore = head;
            Node nodeAfter = head;
            for(int i = 0; i < index + 1; i++){
                if(i == index - 1){
                    nodeBefore = nodeAfter;
                }
                if(i == index){
                    value = nodeAfter.value;
                }
                nodeAfter = nodeAfter.next;
            }
            nodeBefore.next = nodeAfter;
        }
        return value;
    }
    //</editor-fold>

    //<editor-fold desc="toArray()">
    public Object[] toArray1() {
        Object[] array = new Object[size()];
        int i = 0;
        for(E object : this) {
            array[i++] = object;
        }
        return array;
    }

    public Object[] toArray2() {
        Object[] array = new Object[size()];
        Node walker = head;
        for(int i = 0; i < size(); i++){
            array[i] = walker.value;
            walker = walker.next;
        }
        return array;
    }
    //</editor-fold>

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("Haven't gotten here yet");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("Haven't gotten here yet");
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("Haven't gotten here yet");
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("Haven't gotten here yet");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Haven't gotten here yet");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Haven't gotten here yet");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("Haven't gotten here yet");
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("Haven't gotten here yet");
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException("Haven't gotten here yet");
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Haven't gotten here yet");
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("[");
        for(int i=0; i<size(); ++i) {
            string.append(get(i) + ", ");
        }
        return string.substring(0, string.length()-2) + "]";
    }

    private class LinkedListIterator implements Iterator<E> {
        private Node current = new Node(null, head);
        private Node previous = null;
        private boolean okayToRemove = false;

        @Override
        public boolean hasNext() {
            return current.next!=null;
        }

        @Override
        public E next() {
            if(!hasNext()) {
                throw new NoSuchElementException("I'm all tapped out");
            }
            previous = current;
            current = current.next;
            okayToRemove = true;
            return current.value;
        }

        @Override
        public void remove() {
            if(!okayToRemove) {
                throw new IllegalStateException("Cannot remove before calling next()");
            }
            if(current==head) {
                head = head.next;
            } else {
                previous.next = current.next;
                previous = new Node(null, previous);
                current = previous.next;
            }
            okayToRemove = false;
        }
    }
}
