package y17q3.cs2852.msoe.edu;

public interface PureStack<E> {
    E peek();
    E pop();
    void push(E element);
    boolean empty();
}
