package y17q3.cs2852.msoe.edu;

import java.util.*;

public class ArrayList<E> implements List<E>, RandomAccess {
    private E[] data;

    public ArrayList() {
        data = (E[])new Object[0];
    }

    @Override
    public int size() {
        return data.length;
    }

    @Override
    public boolean isEmpty() {
        return size()==0;
    }

    @Override
    public void clear() {
        data = (E[])new Object[0];
    }

    @Override
    public E get(int index) {
        indexOutOfBoundsCheck(index);
        return data[index];
    }

    private void indexOutOfBoundsCheck(int index) {
        if(index<0 || index>=size()) {
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
        }
    }

    @Override
    public E set(int index, E element) {
        indexOutOfBoundsCheck(index);
        E prevValue = get(index);
        data[index] = element;
        return prevValue;

    }

    @Override
    public boolean add(E element) {
        E[] temp = (E[])new Object[size()+1];
        for(int i=0; i<size(); ++i) {
            temp[i] = data[i];
        }
        temp[size()] = element;
        data = temp;
        return true;
    }

    @Override
    public boolean contains(Object target) {
        return indexOf(target)>=0;
    }

    @Override
    public void add(int index, E element) {
        if(index<0 || index>size()) {
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
        }
        E[] temp = (E[])new Object[size()+1];
        for(int i=0; i<index; ++i) {
            temp[i] = data[i];
        }
        temp[index] = element;
        for(int i=index; i<size(); ++i) {
            temp[i+1] = data[i];
        }
        data = temp;
    }

    @Override
    public E remove(int index) {
        E removedElement = get(index);
        E[] temp = (E[])new Object[size()-1];
        for(int i=0; i<index; ++i) {
            temp[i] = data[i];
        }
        for(int i=index+1; i<size(); ++i) {
            temp[i-1] = data[i];
        }
        data = temp;
        return removedElement;
    }

    @Override
    public int indexOf(Object target) {
        int index = -1;
        for(int i=0; index==-1 && i<size(); ++i) {
            if((target==null ? data[i]==null : target.equals(data[i]))) {
                index = i;
            }
        }
        return index;
    }

    @Override
    public boolean remove(Object target) {
        int index = indexOf(target);
        if(index>=0) {
            remove(index);
        }
        return index>=0;
    }

    @Override
    public Object[] toArray() {
        return data;
    }

    @Override
    public Iterator<E> iterator() {
        return new ArrayListIterator();
    }

    private class ArrayListIterator implements Iterator<E> {

        private int position = -1;
        boolean okayToRemove = false;

        @Override
        public boolean hasNext() {
            return position+1<size();
        }

        @Override
        public E next() {
            ++position;
            okayToRemove = true;
            return get(position);
        }

        @Override
        public void remove() {
            if(okayToRemove) {
                okayToRemove = false;
                ArrayList.this.remove(position);
            } else {
                throw new IllegalStateException("Cannot remove without calling next first");
            }

        }
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("too lazy to implement");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("too lazy to implement");
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("too lazy to implement");
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("too lazy to implement");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("too lazy to implement");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("too lazy to implement");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("too lazy to implement");
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("too lazy to implement");
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException("too lazy to implement");
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("too lazy to implement");
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("[");
        for(int i=0; i<size(); ++i) {
            string.append(get(i) + ", ");
        }
        return string.substring(0, string.length()-2) + "]";
    }
}
