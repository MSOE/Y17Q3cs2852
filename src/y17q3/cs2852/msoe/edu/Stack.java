package y17q3.cs2852.msoe.edu;

import java.util.EmptyStackException;
import java.util.List;

public class Stack<E> implements PureStack<E> {
    private List<E> data;

    public Stack() {
        data = new LinkedList<>();
    }

    @Override
    public E peek() {
        if(empty()) {
            throw new EmptyStackException();
        }
        return data.get(0);
    }

    @Override
    public E pop() {
        if(empty()) {
            throw new EmptyStackException();
        }
        return data.remove(0);
    }

    @Override
    public void push(E element) {
        data.add(0, element);
    }

    @Override
    public boolean empty() {
        return data.isEmpty();
    }
}
