package y17q3.cs2852.msoe.edu;

import java.util.*;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class HashTable<E> implements Set<E> {
    private LinkedList<E>[] buckets;

    private static final int DEFAULT_SIZE = 19;

    public HashTable() {
        this(DEFAULT_SIZE);
    }

    public HashTable(int numberOfBuckets) {
        buckets = new LinkedList[numberOfBuckets];
    }

    @Override
    public boolean isEmpty() {
        return Arrays.stream(buckets)
                .filter(l -> l!=null)
                .count() == 0;
    }

    @Override
    public int size() {
        return Arrays.stream(buckets)
                .filter(Objects::nonNull)
                .mapToInt(List::size)
                .sum();
    }

    @Override
    public boolean contains(Object target) {
        return Arrays.stream(buckets)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .collect(Collectors.toList())
                .contains(target);
    }

    @Override
    public Iterator<E> iterator() {
        return Arrays.stream(buckets)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .iterator();
    }

    @Override
    public Object[] toArray() {
        return Arrays.stream(buckets)
                .filter(Objects::nonNull)
                .flatMap(List::stream)
                .collect(Collectors.toList())
                .toArray();
    }

    @Override
    public boolean add(E element) {
        boolean added = false;
        if(!contains(element)) {
            int index = element==null ? 0
                    : (Math.abs(element.hashCode())%buckets.length);
            if (buckets[index] == null) {
                buckets[index] = new LinkedList<>();
            }
            added = buckets[index].add(element);
        }
        return added;
    }

    @Override
    public boolean remove(Object target) {
        int index = target==null ? 0
                : (Math.abs(target.hashCode())%buckets.length);
        return buckets[index]!=null && buckets[index].remove(target);
    }

    @Override
    public void clear() {
        buckets = new LinkedList[DEFAULT_SIZE];
    }

    @Override
    public <T> T[] toArray(T[] array) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException("Not implemented");
    }
}
