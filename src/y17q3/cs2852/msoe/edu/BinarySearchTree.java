package y17q3.cs2852.msoe.edu;

import java.util.NoSuchElementException;

public class BinarySearchTree<E extends Comparable<E>> {
    private Node<E> root;
    private static class Node<E extends Comparable<E>> {
        E value;
        Node<E> leftChild;
        Node<E> rightChild;
        Node<E> parent;

        Node(E value, Node<E> leftChild, Node<E> rightChild, Node<E> parent) {
            this.value = value;
            this.leftChild = leftChild;
            this.rightChild = rightChild;
            this.parent = parent;
        }

        Node(E value, Node<E> leftChild, Node<E> rightChild) {
            this(value, leftChild, rightChild, null);
        }

        Node(E value) {
            this(value, null, null);
        }
    }

    private Node<E> rightRotate(Node<E> z) {
        /*
                    Right Rotate on z
                    =================
                   z                y
                 /  \             /  \
                y   D   RR       x    z
              /  \     ---->    /\   /\
             x   C       z     A B  C D
            /\
           A B
         */
        Node<E> y = z.leftChild;
        if(y==null) {
            throw new IllegalArgumentException("Node " + z + " must have a left child.");
        }
        Node<E> C = y.rightChild;
        z.leftChild = C;
        y.rightChild = z;
        if(C!=null) {
            C.parent = z;
        }
        y.parent = z.parent;
        if(z.parent!=null) {
            if(z.parent.rightChild==z) {
                z.parent.rightChild = y;
            } else {
                z.parent.leftChild = y;
            }
        }
        z.parent = y;
        return y;
    }

    public boolean add(E value) {
        boolean added = false;
        if(value==null) {
            throw new IllegalArgumentException("Data structure does not allow null elements");
        }
        if(root==null) {
            root = new Node(value);
            added = true;
        } else {
            added = add(root, value);
        }

        return added;
    }

    private boolean add(Node<E> subroot, E value) {
        boolean added = false;
        if(!value.equals(subroot.value)) {
            if(subroot.value.compareTo(value)<0) {
                if(subroot.rightChild==null) {
                    subroot.rightChild = new Node(value);
                    added = true;
                } else {
                    added = add(subroot.rightChild, value);
                }
            } else {
                if(subroot.leftChild == null) {
                    subroot.leftChild = new Node(value);
                    added = true;
                } else {
                    added = add(subroot.leftChild, value);
                }
            }

        }
        return added;
    }

    public int height() {
        return height(root);
    }

    private int height(Node<E> subroot) {
        int height = 0;
        if(subroot!=null) {
            height = 1 + Math.max(height(subroot.leftChild), height(subroot.rightChild));
        }
        return height;
    }

    public boolean contains(E target) {
        return contains1(root, target);
    }

    private boolean contains(Node<E> subroot, E target) {
        boolean found = false;
        if(subroot!=null) {
            if(subroot.value.equals(target)) {
                found = true;
            } else if(target.compareTo(subroot.value)<0) {
                found = contains(subroot.leftChild, target);
            } else {
                found = contains(subroot.rightChild, target);
            }
        }
        return found;
    }

    private boolean contains1(Node<E> subroot, E target) {
        if(subroot==null) {
            return false;
        }

        if(subroot.value.equals(target)) {
            return true;
        }

        if(target.compareTo(subroot.value)<0) {
            return contains1(subroot.leftChild, target);
        }

        return contains1(subroot.rightChild, target);
    }

    @Override
    public String toString() {
        return "[" + toString(root).substring(2) + "]";
    }

    private String toString(Node<E> subroot) {
        if(subroot==null) {
            return "";
        }
        return toString(subroot.leftChild) + ", " + subroot.value
                + toString(subroot.rightChild);
    }

    public E max() {
        if(root==null) {
            //return null;
            throw new NoSuchElementException("Empty trees are element free");
        }
        Node<E> subroot = root;
        while(subroot.rightChild!=null) {
            subroot = subroot.rightChild;
        }
        return subroot.value;
    }

    public E stupidMaxThatStudentsShouldBeAbleToDoWithoutMyHelp() {
        if(root==null) {
            //return null;
            throw new NoSuchElementException("Empty trees are element free");
        }
        return max(root);
    }

    private E max(Node<E> subroot) {
        return subroot.rightChild==null ? subroot.value
                : max(subroot.rightChild);
    }
}
