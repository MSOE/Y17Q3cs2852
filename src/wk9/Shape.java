package wk9;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;

import java.util.Arrays;

public class Shape implements Cloneable {
    private Point2D center;
    private Color color;
    private char[] name;

    public Shape(double x, double y, Color color, String name) {
        this(new Point2D(x, y), color, name);
    }

    public Shape(Point2D point, Color color, String name) {
        center = point;
        this.color = color;
        this.name = new char[name.length()];
        for(int i=0; i<name.length(); ++i) {
            this.name[i] = name.charAt(i);
        }
    }

    public Shape(Shape original) {
        center = new Point2D(original.center.getX(), original.center.getY());
        color = original.color;
        name = Arrays.copyOf(original.name, original.name.length);
    }

    @Override
    public Shape clone() {
        Shape shape = null;
        try {
            shape = (Shape)super.clone();
            shape.center = new Point2D(center.getX(), center.getY());
            shape.name = Arrays.copyOf(name, name.length);
        } catch(CloneNotSupportedException e) {
        }
        return shape;
    }
    // ...
//<editor-fold desc="Surprising stuff hidden here">

    public void setX(double x) {
        center.subtract(center.getX(), 0);
        center.add(x, 0);
    }

    public void setY(double y) {
        center.subtract(0, center.getY());
        center.add(0, y);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Shape named " + toString(name) + " at ("
                + center.getX() + ", " + center.getY()
                + ") with color: (" + color + ")";
    }

    private static String toString(char[] name) {
        StringBuilder string = new StringBuilder();
        for(Character c : name) {
            string.append(c);
        }
        return string.toString();
    }
//</editor-fold>














    //public Shape(Shape shape) {
    //    this(shape.center, shape.color, toString(shape.name));
    //}
}
