package wk9;

import javafx.geometry.Dimension2D;
import javafx.scene.paint.Color;

public class Rectangle extends Shape {
    private Dimension2D size;

    public Rectangle(double x, double y, Color color, String name,
                     double width, double height) {
        super(x, y, color, name);
        size = new Dimension2D(width, height);
    }

    public Rectangle(Rectangle original) {
        super(original);
        size = new Dimension2D(original.size.getWidth(),
                original.size.getHeight());
    }

    @Override
    public String toString() {
        return "(" + size.getWidth() + ", " + size.getHeight()
                + ") Rectangle"
                + super.toString().substring("Shape".length());
    }

    public void setSize(double width, double height) {
        size = new Dimension2D(width, height);
    }
}
